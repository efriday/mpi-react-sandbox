export interface PetAPIResponseBody {
    numberOfResults: number;
    startIndex:      number;
    endIndex:        number;
    hasNext:         boolean;
    pets:            Pet[];
}

export interface Pet {
    id:          number;
    name:        string;
    animal:      Animal;
    city:        string;
    state:       string;
    description: string;
    breed:       string;
    images:      string[];
}

export enum Animal {
    Cat = "cat",
    Dog = "dog",
    Rabbit = "rabbit",
    Bird = "bird",
    Reptile = "reptile",
}

export default class CatsService {
    async getCats(count: number = 5): Promise<Pet[]> {
        const res = await fetch('https://pets-v2.dev-apis.com/pets?animal=cat').then(res => res.json() as Promise<PetAPIResponseBody>);
        return res.pets.slice(0, (count - 1));
    }

    async getPets(): Promise<Pet[]> {
        return await fetch('https://pets-v2.dev-apis.com/pets').then(res => res.json() as Promise<PetAPIResponseBody>).then(body => body?.pets);
    }
}
