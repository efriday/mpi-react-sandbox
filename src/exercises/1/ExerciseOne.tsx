import { dogList, Pet } from "../../data/pets";
import "./example-card-styling.css";

export default function ExerciseOne() {
  // Think of the ExerciseOne component as the Domain Component which would have handled fetching the data fo ryou.
  const exerciseData = dogList;

  return (
    <article>
      <header>
        <hgroup>
          <h3>Exercise 1 - Beyond "Hello World!"</h3>
          <p>
            Use the code located in the file 'ExerciseOne.tsx' to create a component that renders a list of pets
            on the screen.
          </p>
        </hgroup>
      </header>
      {/* 
        NOTE: Your content should show in the element below named PetList. 
        The mock data is being provided through Props.
        */}
      <PetList pets={exerciseData} />
    </article>
  );
}

/**
 * EDIT THE BELOW CODE TO MAKE IT WORK
 */
//
interface PetListProps {
  pets: Pet[];
}
/**
 * The PetList component should take in a list of pets in its props and then render a list of the PetCards components for each one. 
 * It is responsible for handling "the list" and the HTML related to the list.
 */
function PetList(props: PetListProps) {
  return (
    <section>
      <div>
        <mark>TODO</mark>: Make this show a list of pets.&nbsp;
        <span data-tooltip="Solution code is available in the file named `SolutionExerciseOne`.">
          💡
        </span>
      </div>
      <PetCard TODO={props.pets[0]} />
      <h4>Example Layout Cards:</h4>
      <div className="container grid">
        {/* TODO: You should remove the `<ReferenceCardWithStyling` and replace it with your own component. */}
        {[1,2,3].map((i) => <ReferenceCardWithStyling key={i}/>)}
      </div>
    </section>
  );
}

interface PetCardProps {
  // TODO Try passing props down in different ways.
  TODO: any;
}
/**
 * The PetCard component should show information about an individual pet such as name, age, type of animal, and favorite toy.
 */
function PetCard(props: PetCardProps) {
  return (
    <>
      <p>
        Current Pet Data: <code>{JSON.stringify(props.TODO)}</code>
      </p>
    </>
  );
}

/**
 * This card exists for your reference. It's a template so you don't have to get mucked down in CSS and layout concerns.
 * Feel free to experiement and display it however you like.
 */
function ReferenceCardWithStyling(props: any) {
  // Normally, this data would come from the props or somewhere else. It's just here to give you an example.
  const petData: Pet = {
    name: "Lucy",
    age: 14,
    animal: "dog",
    favoriteToy: "ball",
  };

  return (
    <div className="pet-card">
      <h3>{petData.name}</h3>
      <p>
        <em>{petData.animal}</em>
      </p>
      <p>Age: {petData.age}</p>
      <p>Favorite Toy: {petData.favoriteToy}</p>
    </div>
  );
}
