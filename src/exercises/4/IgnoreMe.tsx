import React from "react";

export default function ExerciseFour() {
    return (
        <article>
          <header>
            <hgroup>
              <h3>Exercise Four - Layouts </h3>
              <p>
                The code for the containers <em>holding the exercises</em> is getting awfully repetative. We should refactor it and make it more reusable.
              </p>
            </hgroup>
          </header>
          {/* 
            NOTE: Your content should show in the element below named CatContainer. 
            The mock data is being provided in that component.
            */}
          <ExerciseLayout title="title" description="description" ProjectedComponent={<div>Children components go here.</div>} />
        </article>)
}

function ExerciseHOC(WrappedComponent: any){ 
    return function(props: any) {
        return (<WrappedComponent {...props} />);
    }
}

function ExerciseLayout({ title, description, ProjectedComponent }: {title: string, description: string, ProjectedComponent: any}) {
    return (
        <article>
          <header>
            <hgroup>
              <h3>{title} </h3>
              <p>
                {description}
              </p>
            </hgroup>
          </header>
          {/* 
            NOTE: Your content should show in the element below named CatContainer. 
            The mock data is being provided in that component.
            */}
          <ProjectedComponent />
        </article>);
}
