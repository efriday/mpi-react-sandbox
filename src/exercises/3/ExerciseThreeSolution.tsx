import React, { useEffect } from "react";

export default function ExerciseThreeSolution() {
    return (
        <article>
            <header>
                <hgroup>
                    <h3>Exercise Three - Higher Order Components</h3>
                    <p>
                        Use the code below to create a component that renders a
                        list of cats to the page.
                        <br />
                        The component should <code>console.log</code> the name
                        of each pet as <em>at the time it is rendered</em>.
                    </p>
                </hgroup>
            </header>
            {/* 
                NOTE: Your content should show in the element below named CatContainer. 
                The mock data is being provided in that component.
            */}
            <CatContainer />
        </article>
    );
}

const catNames = [
    "Luna",
    "Mittens",
    "Basil",
    "Nibbles",
    "Izzy",
    "Oliver",
    "Leo",
    "Milo",
    "Simba",
    "Loki",
    "Zoe",
    "Lola",
];

function CatContainer() {
    return (
        <section className="grid">
        {/* 
            Here is the first change. Notice I haven't had to alter the `CatCard` component at all. 
            Instead, I've 'wrapped' the component with a function declared below.
        */}
            {[1, 2, 3, 4, 5].map((i) => (
                <CatCardWithLogging key={i} catName={catNames[i]} />
            ))}
        </section>
    );
}

function CatCard({ catName }: { catName: string }) {
    return (
        <div className="cat-card">
            <img src="https://placekitten.com/200/200" alt="A Cat" />
            <h4>Look at this adorable Cat.</h4>
            <p>It's really cute, and it's name is {catName}</p>
        </div>
    );
}

/**
 * Solution Details:
 * First off, you'll notice that the names are being logged twice to the console. 
 * This is expected because React runs in `Strict Mode` during development.
 * A full explanation with details is here:
 * https://reactjs.org/docs/strict-mode.html
 *
 * For the actual implementation, I've written a custom hook that logs the props. 
 * But we need to be able to tell that hook what props we want to log. This would
 * likely be unique to each component it wraps, and may change depending on the context, so this should be dynamic.
 *
 * To get around that, I've created a nested function named `withLogPropsHook`.
 * - The first component takes the component we want to wrap and "remembers it" in a closure.
 * - The second component takes the props we want to log and "remembers" them in a closure. This closure has access to its parent's closure (with the component).
 * - Lastly, it returns the `DoLogProps` function which will be called by React when it renders the component.
 * - - This function will automatically add the `useEffect` block we have below, which logs
 *     the props we choose that are being "remembered" in a parent scope -- e.g. inside the `choosePropsToLog` function declared above it.
 *
 * There are many different ways we could structure this. Notice that the function doesn't know anything about the CatCard, the props it takes, or
 * anything else. This makes it available for re-use at the core level.
 * Additionally, separating the logic out between functions allows us to re-use the behavior at different levels.
 * For example, If we re-used the CatCard but wrapped it in a HOC that added a button to click and make it a "favorite", we'd still be able
 * to add this hook to it and select a prop from the properties added by the HOC.
 */

const withLogPropsHook = (WrappedComponent: any) => {
    return function choosePropsToLog(propsToLog: string[]) {
        return function DoLogProps(props: any) {
            useEffect(() => {
                if (propsToLog?.length > 0) {
                    propsToLog.forEach((targetProp) =>
                        console.log(
                            `Logging ${targetProp} of ${
                                WrappedComponent.name
                            }: ${props[targetProp] || "invalid"}`
                        )
                    );
                } else {
                    console.log(`No props to log for ${WrappedComponent.name}`);
                }
            }, []);
            return <WrappedComponent {...props} />;
        };
    };
};
// This is where we call `withLogPropsHook` and pass it the component and name of props we'll want to call. 
// NOTE: the `IIFE` syntax at the end allows us to invoke the inner function without having to assign it to a variable.
const CatCardWithLogging = withLogPropsHook(CatCard)(["catName"]);
