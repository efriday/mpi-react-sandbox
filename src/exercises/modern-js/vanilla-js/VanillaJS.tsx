export default function VanillaJS() {
    return (
        <article>
            <header><hgroup><h3>Vanilla JS Challenges</h3><h4>External code challenges To review VanillaJS fundamentals</h4></hgroup></header>
            <details>
                <summary>Asynchronicity & Promises</summary>
                <p><a href="https://csbin.io/promises" className="contrast" target="_blank" rel="noopener noreferrer">https://csbin.io/promises</a></p>
            </details>
            <details>
                <summary>Iterators, Generators & Async/await:</summary>
                <p><a href="https://csbin.io/iterators" className="contrast" target="_blank" rel="noopener noreferrer">https://csbin.io/iterators</a></p>
            </details>
        </article>
    );
}
