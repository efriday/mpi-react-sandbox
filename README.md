# Sandbox Setup

### Local Install

1. Clone the repo and `cd` into it.
2. Install dependencies by running `yarn` or `npm install`.
3. Run `yarn dev` or `npm run dev` to start the development server.
4. Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.
5. It will tell you if you're missing the (React Developer Tools)[https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi], which is incredibly helpful. Install them for your browser if you don't have them.

You're ready to go!

Navigate to the exercise page in your browser, and open the corresponding file in your editor to see all the notes and start editing!
`src/exercises/one/ExerciseOne.tsx` is the entry point for the first exercise.
`src/challenges/patient/ChallengePatient.tsx` is the entry point for the first challenge.

### Notes
Styling is being handled by a minimal semantic-ui inspired CSS library called PicoCSS. Their documentation is here: (PicoCSS)[https://picocss.com/]
For data fetching, we'll just use the browser-based `fetch` capability, but we'll bring in (SWR)[https://swr.vercel.app/docs/getting-started] later to make it easier...
I've provided templates and starter components for many exercises. Feel free to use them, or rewrite them! They're helpful because they'll provide a starting point for the HTML structure of individual components. 
