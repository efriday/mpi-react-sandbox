export default function ScopeAndClosures() {

    return(
    <article>
        <header><hgroup><h3>Scopes & Closures - An Overview</h3></hgroup></header>
        <details>
            <summary role="button">What is scope and closure? Why do we care?</summary>
            <p>Kyle Simpson has an excellent book on this topic. If you feel lost at the words 'scope' and 'closure', then start with <a href="https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/scope-closures/README.md" target="_blank" rel="noopener noreferrer">the book</a>.</p>
            <p><strong>Scope</strong> basically translates to "Where should the code look for things?". If there is a variable <code>widget</code>, then scope refers to the segments of code that variable will be looked for in.</p>
            <ul>
                <li>It's <em>primarily</em> decided during compilation of JS, not during interpretation.</li>
                <li>This type of scope is called "lexical scope" because it is determined during the "lexing" stage of compilation.</li>
                <li>'What to look for' vs 'who is looking for something' are two different questions.</li>
            </ul>
        </details>
    </article>
    )
}
