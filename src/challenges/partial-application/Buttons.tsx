export const partiallyApply = (Component: any, partialProps: any) => {
	return (props: any) => {
		return <Component {...partialProps} {...props} />
	}
}

interface ButtonProps { size: string, color: string, text: string, props: any};
export const Button = function({ size, color, text, ...props }: ButtonProps) {
	return (
		<button style={{
			padding: size === 'large' ? '32px' : '8px',
			fontSize: size === 'large' ? '32px' : '16px',
			backgroundColor: color,
		}} {...props}>{text}</button>
	);
}

export const DangerButton = partiallyApply(Button, { color: 'red' });
export const BigSuccessButton = partiallyApply(Button, { color: 'green', size: 'large' });
