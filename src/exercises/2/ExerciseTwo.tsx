import { useState } from "react";
import ExerciseTwoSolution from "./ExerciseTwoSolution";

export default function ExerciseTwo() {
  return (
    <article>
      <header>
        <hgroup>
          <h3>Exercise 2 - Lifting State & Data Flow</h3>
          <p>Use the code located in 'ExerciseTwo.tsx' to create a solution to the
            following prompt.
          </p>
        </hgroup>
      </header>
      <small>
        Your changes between the two inputs should be synced, and they should
        convert between F and C as appropriate. The text underneath will render
        a different message if the temperature is &gt; 100 degree C.
      </small>
      <br />
      <small>
        Adapted from the official React Docs located{" "}
        <a href="https://reactjs.org/docs/lifting-state-up.html" target="_blank" rel="noopener noreferrer">here.</a>
      </small>
      {/* 
            HINT: You should be thinking about how to lift state up from the TemperatureInput components.
            - Where should the state live? What should the state be?
            - How do we coordinate actions & *events* between multiple components?
            - If you are not sure how to start or what to do, you can check out the link above for a walkthrough of how to approach the problem.
        */}
      {/* Helper functions for temperature conversion are below. */}
      {/* Go ahead and use this component (ExerciseTwo) to act as the container for the other ones.  */}
      <hr />
      <TemperatureInput scale="f" />
      <TemperatureInput scale="c" />
      <BoilingVerdict tempInCelsius={0} />
    </article>
  );
}

function TemperatureInput(props: any) {
  // As you move state up, consider what typings you should use to represent something like a temperature. Be mindful of the return values of input elements.
  const [scale, setScale] = useState<"c" | "f">(props.scale ?? "c");
  const [temperature, setTemperature] = useState(props.temperature ?? "0");

  const handleTemperatureChange = (e: any) => {
    setTemperature(e.target.value);
  };

  return (
    <fieldset>
      <legend>Enter the temperature in {scaleNames[scale]}:</legend>
      <input value={temperature} onChange={handleTemperatureChange} />
    </fieldset>
  );
}

function BoilingVerdict({ tempInCelsius }: { tempInCelsius: number }) {
  if (tempInCelsius >= 100) {
    return <p>The water would boil.</p>;
  }
  return (
    <p>
      The water would <strong>not</strong> boil.
    </p>
  );
}

/**
 * Helper Functions Are Below
 */
function tryConvert(
  temperature: unknown,
  convert: (num: number) => number
): string {
  const input = parseFloat(temperature as string);
  if (Number.isNaN(input)) {
    return "";
  }

  const output = convert(input);
  const rounded = Math.round(output * 1000) / 1000;
  return rounded.toString();
}

const scaleNames = {
  c: "Celsius",
  f: "Fahrenheit",
};

function toCelsius(fahrenheit: number): number {
  return ((fahrenheit - 32) * 5) / 9;
}

function toFahrenheit(celsius: number): number {
  return (celsius * 9) / 5 + 32;
}
