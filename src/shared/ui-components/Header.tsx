import { Link } from "react-router-dom";

export default function Header() {
  return (
    <header>
      <nav className="container">
        <ul>
          <li>
            <Link to="/" className="secondary">
              Home
            </Link>
          </li>
        </ul>
        <ul>
          <li>
            <Link to="/exercises/1" className="secondary">
              💪Exercises💪
            </Link>
          </li>
          <li>
            <Link to="/challenges" className="secondary">
            🔥Challenges🔥
            </Link>
          </li>
          <li>
            <Link to="/modern-js" className="secondary">
              🦄Modern JS Content🦄
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

