import { Pet } from '../../data/pets';
import './example-card-styling.css';

export interface PetListSolutionProps {
    pets: Pet[];
}
export function PetListSolution(props: PetListSolutionProps) {
    return (
        <section>
        <h3>These are my awesome pets!</h3>
        <div className="grid">
            {props.pets.map(
                (pet) => <PetCardSolution 
                name={pet.name}
                age={pet.age}
                animal={pet.animal}
                favoriteToy={pet.favoriteToy}
            />)}
        </div>
        </section>
    );
}
  
export type PetCardSolutionProps ={
    name: string;
    age: number;
    animal: string;
    favoriteToy: string;
}
export default function PetCardSolution({ name, age, animal, favoriteToy }: PetCardSolutionProps) {
    return (
      <div className="pet-card">
        <h3>{name}</h3>
        <p><em>{animal}</em></p>
        <p>Favorite Toy: {favoriteToy}</p>
        <p>Age: {age}</p>
      </div>
    );
  }
  