import { Link, Outlet } from "react-router-dom";

export default function Challenges() {
    return (
        <>
            <details role="list">
                <summary aria-haspopup="listbox">Select A Challenge</summary>
                <ul role="listbox">
                    <li><Link to="/challenges/1" className="secondary">One - Render A Patient</Link></li>
                </ul>
            </details>
            <Outlet />
        </>);
}
