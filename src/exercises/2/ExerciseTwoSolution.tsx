import { useState } from "react";

export default function ExerciseTwoSolution() {
  const [temperatureC, setTemperatureC] = useState("0");
  const temperatureF = handleTemperatureConversion("c")(temperatureC);

  /**
   * We don't need to modify the temp when its already in celsius, so we just pass it along to state. However, I
   * chose to alias the function to make it apparent that it is being used as the handler for celsius value changes.
   */
  const onCelsiusChange = setTemperatureC;
  const onFarenheitChange = (temp: string) =>
    pipe(handleTemperatureConversion("f"), setTemperatureC)(temp);
  /**
   * This syntax is probably unfamiliar. It's equivalent to the below code snippet.
    ```
    const onFarenheitChange = (tempToConvert: string) => {
        const conversionFn = handleTemperatureConversion("f");
        const converted = conversionFn(tempToConvert);
        setTemperatureC(converted);
    };
    ```
    Plain-English walkthrough with details:
    `const onFarenheitChange =` - declare a variable that will hold a resulting function. It's named to convey its purpose as a handler for events.
    `(temp: string) => ` - the function, when finally invoked, will take a temperature as a parameter.
    `pipe(handleTemperatureConversion('f'), setTemperatureC)` - We call the `pipe` function on two other functions in order to compose them together. 
    The first of which is a function that gets the appropriate "F -> C" conversion function and prepares it for our use.
    The second function is the `setTemperatureC` function that we'll use to update the state of the component. *We don't call it here*.
    Think of it as laying out our plans for operating on the 'temp' parameter. 

    NOTE: We invoked the `handleTemperatureConversion` function with 'f', which returned another function. 
    That returned function (named `monoidTryConvert`) is what the `pipe` function actually uses.
    `(temp);` - Finally, we invoke the entire function chain laid out inside the `pipe` function using IIFE syntax.

    You could also write it like this (pseudocode):
    ```
    const convertFarenheitToCelsiusAndUpdateState = pipe(handleTemperatureConversion('f'), setTemperatureC);
    const onFarenheitChange = (temp: string) => convertFarenheitToCelsiusAndUpdateState(temp);
    ```

    There's an incredible number of different places that you can break apart functions and compose them together.
    This is one example used largely to demonstrate currying and function composition.
   */

  return (
    <article>
      <header>
        <hgroup>
          <h3>Exercise Two - Lifting State & Data Flow</h3>
        </hgroup>
      </header>
      <small>
        Your changes between the two inputs should be synced, and they should
        convert between F and C as appropriate. The text underneath will render
        a different message if the temperature is &gt; 100 degree C.
      </small>
      <br />
      <small>
        Adapted from the official React Docs located{" "}
        <a href="https://reactjs.org/docs/lifting-state-up.html" target="_blank" rel="noopener noreferrer">here.</a>
      </small>
      {/* 
            HINT: You should be thinking about how to lift state up from the TemperatureInput components.
            - Where should the state live? What should the state be?
            - How do we coordinate actions & *events* between multiple components?
            - If you are not sure how to start or what to do, you can check out the link above for a walkthrough of how to approach the problem.
        */}
      {/* Helper functions for temperature conversion are below. */}
      {/* Go ahead and use this component (ExerciseTwo) to act as the container for the other ones.  */}
      <hr />
      <TemperatureInput
        temperature={temperatureF}
        scale="f"
        onTempChange={onFarenheitChange}
      />
      <TemperatureInput
        temperature={temperatureC}
        scale="c"
        onTempChange={onCelsiusChange}
      />
      <BoilingVerdict tempInCelsius={parseFloat(temperatureC)} />
    </article>
  );
}

function TemperatureInput({
  temperature,
  scale,
  onTempChange,
}: {
  temperature: string;
  scale: "c" | "f";
  onTempChange: (e: any) => void;
}) {
  return (
    <fieldset>
      <legend>Enter the temperature in {scaleNames[scale]}:</legend>
      <input
        value={temperature}
        onChange={(e) => onTempChange(e.target.value)}
      />
    </fieldset>
  );
}

function BoilingVerdict({ tempInCelsius }: { tempInCelsius: number }) {
  if (tempInCelsius >= 100) {
    return <p>The water would boil.</p>;
  }
  return (
    <p>
      The water would <strong>not</strong> boil.
    </p>
  );
}

/**
 * I rewrote the helper functions to leverage functional patterns, which prevents us from having to
 * track `scale` in state and pass the requested scale data between components.
 * This better isolates it and hides its behavior from the other components.
 *
 * It requires composing a function that takes a scale and returns a function that takes a temperature and returns a string.
 * Essentially, I re-write the `tryConvert` function from accepting two parameters to accepting one parameter and returning
 * a function that accepts one parameter.
 *
 * It's an example of "currying", which is a powerful tool for managing complexity and enabling re-use.
 */
function handleTemperatureConversion(scale: "c" | "f") {
  const conversionFunction = scale === "c" ? toFahrenheit : toCelsius;

  return function monoidTryConvert(temperature: string): string {
    const input =
      typeof temperature === "string" ? parseFloat(temperature) : temperature;
    if (Number.isNaN(input)) {
      return "";
    }

    const output = conversionFunction(input);
    const rounded = Math.round(output * 1000) / 1000;
    return rounded.toString();
  };
}

const pipe =
  (...fns: Function[]) =>
  (x: unknown) =>
    fns.reduce((y, f) => f(y), x);

/**
 * Helper Functions Are Below
 */
function tryConvert(
  temperature: unknown,
  convert: (num: number) => number
): string {
  const input = parseFloat(temperature as string);
  if (Number.isNaN(input)) {
    return "";
  }

  const output = convert(input);
  const rounded = Math.round(output * 1000) / 1000;
  return rounded.toString();
}

const scaleNames = {
  c: "Celsius",
  f: "Fahrenheit",
};

function toCelsius(fahrenheit: number): number {
  return ((fahrenheit - 32) * 5) / 9;
}

function toFahrenheit(celsius: number): number {
  return (celsius * 9) / 5 + 32;
}
