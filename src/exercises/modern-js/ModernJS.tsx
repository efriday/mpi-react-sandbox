import { Link, Outlet } from "react-router-dom";

export default function ModernJS(){
    return (
        <article>
            <nav>
                <ul>
                    <li><Link to="/modern-js">Overview</Link></li>
                    <li><Link to="/modern-js/vanilla">Vanilla JS Refreshers</Link></li>
                </ul>
            </nav>
            <Outlet />
        </article>
    )
}
