import { useEffect, useState } from "react";
import useSWR from 'swr';
import Spinner from "../../shared/ui-components/Spinner";
// import ExerciseFiveSolution from "./ExerciseFiveSolution";

export default function ExerciseFive() {
    return (        
    <article>
        <header>
            <hgroup>
                <h3>Exercise 5 - Live APIs, Destructuring, & Composition</h3>
            </hgroup>
        </header>
        <details>
            <summary role="button">Click Here for Details & Specification</summary>
            <p>Oh no! <em>Business</em> just told us that there'd be a change in requirements for our beloved Cat app! Now, we need to support a diverse array of pets.</p>
            <p>Additionally, this list of pets is being provided by a vendor, so we'll need to hit their API to fetch them.</p>
            <p>For data-fetching, we'll bring in a special library which helps us keep our code more declarative while handling data fetching responsibilities. It's name is: <strong>SWR</strong>.</p>
            <small>Start at the "Quick Start" portion of "Getting Started" for an overview: <a href="https://swr.vercel.app/docs/getting-started#quick-start" target="_blank" rel="noopener noreferrer">https://swr.vercel.app/docs/getting-started#quick-start</a></small>
            {/* Don't worry about file structure layout for now. Just keep it all in one file and go for it. */}
        </details>

        
        {/* 
        TODO: REPLACE THE FOLLOWING COMPONENT WITH YOUR OWN IMPLEMENTATION
        Pet Container Naive shows you.... the naive approach. 
        - Repeated data fetching on multiple-component instantiation
        - lack of re-use and portability.
        - no error handling
        */}
        <PetContainerNaive />
        {/* <ExerciseFiveSolution /> */}
    </article>)
}

/**
 * interface for the `pets` array present on the response body.
 */
export interface Pet {
    id:          number;
    name:        string;
    animal:      string;
    city:        string;
    state:       string;
    description: string;
    breed:       string;
    images:      string[];
}

/**
 * This demonstrates the most basic way to have a component fetch some data and then trickle it down to components underneath it.
 */
function PetContainerNaive() {
    // TODO: Refactor how we handle data fetching. You'll need to re-consider state handling, and move the effect to somewhere that makes more sense.
    const [pets, setPets] = useState<Pet[] | null>(null);

    // We'd probably want to re-use this data fetching & handling logic at some point... But it's pretty tied in here.
    useEffect(() => {
        fetch('https://pets-v2.dev-apis.com/pets')
            .then(res => res.json())
            .then((body) => body?.pets)
            .then(data => setPets(data))
            .catch(err => console.error(err)); // Notice the error isn't being handled.
    }, []);

    if (!pets) return (<Spinner/>); // loading spinner
    // What do we do about error handling? Introduce a new piece of state? A new variable? A new component?
    return (
    <>
        <h3>Naive implementation of pets</h3>
        <p>We found {pets.length} pets available!</p>
        <PetList pets={pets} />
    </>);
}

function PetList({ pets }: { pets: Pet[] }) {
    if (!(pets.length > 0 )) {
        return (<article><h3>There are no pets to display.</h3></article>)
    }
    return (
    <ul>
        {  pets.map((pet: Pet) => <PetCard key={pet?.id} {...pet}/>) }
    </ul>
    );
}
/**
 * SWR code hints: I'll provide the `fetcher` wrapper function, which would normally be provided through the app.
 * The `usePets` function should be a hook that uses `useSWR` to fetch the data.
 * `usePets` should expose the ability to get the pets and know if the call is either still pending or has errored.
 * I've gone ahead and plugged in the URL and fetcher helper function. You'll need to unpack it and interpret it.
 */
const fetcher = (...args: [input: RequestInfo | URL, init?: RequestInit | undefined]) => fetch(...args).then(res => res.json())
function usePets() {
    const hint = useSWR('https://pets-v2.dev-apis.com/pets', fetcher);
    return {};
}

/**
 * Re-use this component as-is. It will do the trick.
 */
 export function PetCard(props: Pet) {
    /*
    The following line uses nested destructuring to pull out the properties we need from the props object.
    If you're not sure how the variable `imgSrc` is being declared and/or assigned, then this is a good opporunity to learn more about the syntax!
    */
    const { name, animal, breed, images: [imgSrc], city, state, id, description } = props;
    const location = `${city}, ${state}`;

    return (
        <article>
            <header>
                <hgroup>
                    <h3>{name} the {animal} ({breed})</h3>
                    <p><em>{location}</em></p>
                </hgroup>
            </header>
            {imgSrc ? <img src={imgSrc} alt={`Photo of ${name}`} style={ {'height':'200px', 'width':'200px' } }/> : null}
            <br/>
            <small>{description}</small>
        </article>
    )
}
