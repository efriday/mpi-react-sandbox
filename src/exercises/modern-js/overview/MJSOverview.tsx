export default function MJSOverview() {
    const gatherOperatorTooltipText = `\`first\` would evaluate to 1. \`theRest\` would be an array containing the remaining values: \`[2, 3, 4]\``;
    const nullishTooltipText = `The nullish coalescing operator (??) returns the right hand value if the left hand expression is \`null\` or \`undefined\`. It avoids coersion unlike the || operator.`;
    const optionalChainingTooltipText = `Optional Chaining is relatively straightforward for property access, but not function calls.`

    return (
        <details open>
            <summary>Things to know about JS:</summary>
            <ul>
                <li>Object Destructuring</li>
                <li>Template Literals</li>
                <li>Shorthand Property Names</li>
                <li>Arrow Functions</li>
                <li>Parameters defaults</li>
                <li>Rest/Spread/Gather operator - <code data-tooltip={gatherOperatorTooltipText}>var [first, ...theRest] = [1, 2, 3, 4];</code></li>
                <li>ESModules</li>
                <li>Promises and <code>async</code> /<code>await</code></li>
                <li>Array Methods: <code>find, some, every, includes, map, filter, reduce, flat, flatMap</code></li>
                <li>Nullish coalescing operator - e.g. <code data-tooltip={nullishTooltipText}>var value = possiblyValidValue ?? 'default'</code></li>
                <li>Optional Chaining </li>
                <ul>
                    <li>e.g. <code>var value = thing?.prop?.val;</code></li>
                    <li>e.g. <code>var funcRef = options?.onSuccess;</code>&nbsp;&nbsp;Followed by:&nbsp;&nbsp;<code data-tooltip={optionalChainingTooltipText}>funcRef?.(props);</code></li>
                </ul>
            </ul>
            <small>Source:&nbsp;<a href="https://kentcdodds.com/blog/javascript-to-know-for-react" target="_blank" rel="noopener noreferrer">JS To Know For React</a> by Kent C. Dodds</small>
        </details>
    );
}
