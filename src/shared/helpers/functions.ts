export const compose = (...fns: Function[]) => (x: unknown) => fns.reduceRight((y: unknown, f) => f(y), x);
export const pipe =
  (...fns: Function[]) =>
  (x: unknown) =>
    fns.reduce((y, f) => f(y), x);

export function randomKey() {
    let randomNum = getRandomArbitrary(1, 1_000_000);
    let num = randomNum.toString();
    let [first] = num.split('.');
    console.log(`Generated random number: ${first}`);
    return first;
  }
  
export function getRandomArbitrary(min: number, max: number) {
    return Math.random() * (max - min) + min;
}
