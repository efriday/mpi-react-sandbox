import React, { useState } from "react";
import { Cat, catList } from "../../data/pets";
import { randomKey } from "../../shared/helpers/functions";

export default function ExerciseFour() {
    return (
        <article>
            <header>
                <hgroup>
                    <h3>Exercise 4 - Forms & More Complex State </h3>
                    <p>Using the code in 'ExerciseFour.tsx', allow users to add a pet to the list of cats!</p>
                </hgroup>
            </header>
            {/* 
            NOTE: Your content should show in the element below named CatContainer. 
            The mock data is being provided in that component.
            */}
            <CatContainer />
        </article>
    );
}

/**
 *
 * Cat Object Shape - { name: string, animal: string, age: number, favoriteToy: string }
 */

function CatContainer() {
    const [cats, setCats] = useState(catList);

    return (
        <>
            <AddPetForm onAddPet={(pet: Cat) => setCats([...cats, pet])} />
            <section className="grid">
                {cats.map((cat: Cat) => (
                    <CatCard key={randomKey()} {...cat} />
                ))}
            </section>
        </>
    );
}

function CatCard({ name, animal, age, favoriteToy }: Cat) {
    return (
        <article>
            <img src="https://placekitten.com/200/200" alt="A Cat" />
            <h5>{name} - {animal}</h5>
            <p>
                Age: {age} years
                <br />
                Favorite Toy: {favoriteToy}
            </p>
        </article>
    );
}

/**
 * TODO: Update the AddPetForm so it gets values out of the form properly and passes them back up to the container.
 * The `onAddPet` prop is a big hint on how you can do this. 
 */
function AddPetForm({ onAddPet }: { onAddPet: (pet: Cat) => void }) {
    const [petName, setPetName] = useState("");
    const [petAge, setPetAge] = useState(0);
    const [petFavoriteToy, setPetFavoriteToy] = useState("");

    /**
     * TODO: Add implementations.
     */
    const handleChange = function ChangeHandler(event: any) {
        console.error("TODO: Implement the handleChange function");
    };
    const TODO = handleChange;

    const handleReset = function ResetHandler(event: any) {
        /**
         * Handle form resets
         */
        throw new Error('TODO: Implement the handleReset function');
    };
    const handleSubmit = function SubmitHandler(event: any) {
        /**
         * TODO: handle the submit. 
         * NOTE: I'll give you a head start by calling `event.preventDefault()` because the browsers default behavior on 
         * form submission is to redirect, which we don't want.
         */
        event.preventDefault();
        throw new Error('TODO: Implement the handleSubmit function');
    };

    return (
        <section id="add-pet">
            <form onSubmit={handleSubmit} className="pet-form">
                <h4>Share your pet with the world!</h4>
                <label htmlFor="petName">Pet Name</label>
                <input
                    type="text"
                    name="petName"
                    placeholder="Enter A Name"
                    onChange={TODO}
                    required
                />
                <label htmlFor="petAge">Age</label>
                <input
                    type="number"
                    name="petAge"
                    onChange={TODO}                    
                    required
                />
                <label htmlFor="petFavoriteToy">Favorite Toy</label>
                <input
                    type="text"
                    name="petFavoriteToy"
                    onChange={TODO}                    
                    required
                />
                <div className="grid">
                    <input type="reset" value="Reset" onClick={handleReset} />
                    <input type="submit" value="Add Pet"></input>
                </div>
            </form>
        </section>
    );
}
