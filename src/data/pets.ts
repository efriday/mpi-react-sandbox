// type Pet = { name: string, animal: string, age: number, favoriteToy: string };
type AnimalDetails = { name: string, animal: string, age: number, favoriteToy: string };

export type Dog = AnimalDetails & { animal: 'dog' };
export type Cat = AnimalDetails & { animal: 'cat' };
export type Baby = AnimalDetails & { animal: 'baby' };
export type Fish = AnimalDetails & { animal: 'fish' };
export type Pet = Dog | Cat | Baby | Fish;

/**
 * Dog Stuff
 */
const lucy: Dog = { name: 'Lucy', animal: 'dog', age: 14, favoriteToy: 'A Squeaky Felt Duck' };
const fido: Dog = { name: 'Fido', animal: 'dog', age: 5, favoriteToy: 'Bone' };
const mildred: Dog = { name: 'Mildred', animal: 'dog', age: 2, favoriteToy: 'Frisbee' };
const littleBit: Dog = { name: 'Lil Bit', animal: 'dog', age: 8, favoriteToy: 'Tennis Ball' };

export const dogList: Dog[] = [lucy, fido, mildred, littleBit];

/**
 * Cat Stuff
 */
const izzy: Cat = { name: 'Izzy', animal: 'cat', age: 13, favoriteToy: 'A Piece of String' };
const basil: Cat = { name: 'Basil', animal: 'cat', age: 13, favoriteToy: 'Knitted Mouse' };
const mittens: Cat = { name: 'Mittens', animal: 'cat', age: 4, favoriteToy: 'Shoe Strings' };

export const catList: Cat[] = [izzy, basil, mittens];

/**
 * Baby stuff
 */
const rowan: Baby = { name: 'Rowan', animal: 'baby', age: 1, favoriteToy: 'Rattle' };

export const babyList: Baby[] = [rowan];

/**
 * fish stuff
 */
const ole: Fish = { name: 'Ole', animal: 'fish', age: 1, favoriteToy: 'A Rock' };

export const myPetArray: Pet[] = [...dogList, ...catList];

/**
 * IGNORE
 */

 const catNames = ['Luna', 'Mittens', 'Basil', 'Nibbles', 'Izzy', 'Oliver', 'Leo', 'Milo', 'Simba', 'Loki', 'Zoe', 'Lola'];
