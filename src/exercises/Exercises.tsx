import { Link, Outlet } from "react-router-dom";

export default function Exercises() {
    return (
        <>
            <details role="list">
                <summary aria-haspopup="listbox">Select An Exercise</summary>
                <ul role="listbox">
                    <li><Link to="/exercises/1" className="secondary">1 - Rendering Props</Link></li>
                    <li><Link to="/exercises/2" className="secondary">2 - The <code>useState</code> Hook</Link></li>
                    <li><Link to="/exercises/3" className="secondary">3 - Higher Order Components & Sharing Behavior</Link></li>
                    <li><Link to="/exercises/4" className="secondary">4 - Forms & Moderately Complex State</Link></li>
                    <li><Link to="/exercises/5" className="secondary">5 - Live APIs, Destructuring, & Composition</Link></li>
                </ul>
            </details>
            <Outlet />
        </>
    );
}
