import { patient, patients } from './data';

/**
 * You have a FHIR patient resource available to you exposed as `patient` and `patients` (an array of patients).
 * Create a series of components that will display the patient data in a way that is easy to read and understand.
 * An example of an existing library is available here:
 * (https://github.com/healthintellect/fhir-ui/tree/master/src/components/patient)[https://github.com/healthintellect/fhir-ui/tree/master/src/components/patient]
 * 
 * However the code is problematic. Consider your own implementation and how you might improve it.
 * Good luck!
 */
export default function ChallengePatient() {
    return(
        <article>
            <h1>Challenge One - Render A Complex Patient</h1>
            <p>Please see the code for instructions. <code>src/challenges/patient/ChallengePatient.tsx</code></p>
            <p>Example of existing UI for FHIR components. <a href="https://github.com/healthintellect/fhir-ui/tree/master/src/components/patient" target="_blank" rel="noopener noreferrer">HealthIntellect - FHIR UI</a></p>
        </article>
    );
}
