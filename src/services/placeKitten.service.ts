const placeKittenUrl = 'https://placekitten.com';

export class PlaceKittenAPI {
    static async fetchKittenPictureRectangle(width: number = 150, height: number = 150) {
        return fetch(`${placeKittenUrl}/${width}/${height}`);
    }
    public static async fetchKittenPicture(width: number = 150, height?: number) {
        if (height) {
            return PlaceKittenAPI.fetchKittenPictureRectangle(width, height);
        }
        return PlaceKittenAPI.fetchKittenPictureRectangle(width, width);
    }
}