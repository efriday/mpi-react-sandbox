import React, { useEffect } from "react";

export default function ExerciseThree() {
    return (
    <article>
      <header>
        <hgroup>
          <h3>Exercise 3 - Higher Order Components & Sharing Behavior</h3>
          <p>
            Use the code located in 'ExerciseThree.tsx' to create a component that renders a list of cats to the page.<br/>
            The component should <code>console.log</code> the name of each pet as <em>at the time it is rendered</em>.
          </p>
        </hgroup>
      </header>
      {/* 
        NOTE: Your content should show in the element below named CatContainer. 
        The mock data is being provided in that component.
        */}
      <CatContainer />
    </article>)
}

const exampleCatNames = ['Luna', 'Basil', 'Izzy', 'Oliver', 'Milo'];

function CatContainer() {
    const [catNames, setCatNames] = React.useState(exampleCatNames);
  return (
    <section className="grid">
        {/* 
            Ignore the `key` attribute for now. If you're curious, read about them (here)[https://reactjs.org/docs/lists-and-keys.html]
        */}
        {catNames.map((catName, idx) => <CatCard key={`${idx}-${catName}`} catName={catName}/>)}
    </section>
  );
}

/** 
 * Go ahead and use this function as your display template.
 * You should *not* have to change it to have a working solution.
 **/
function CatCard({ catName }: { catName: string }) {

  return (
    <div className="cat-card">
        <img src="https://placekitten.com/200/200" alt="A Cat" />
        <h4>Look at this adorable Cat.</h4>
        <p>It's really cute, and it's name is {catName}</p>
    </div>);
}

/**
 * TODO: write a custom hook that wraps a component and logs props that you specify.
 */
const withLogPropsHook = (WrappedComponent: any) => {
    console.error('TODO');
}
