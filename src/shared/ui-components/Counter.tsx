import { useState } from "react";

function Counter() {
    const [count, setCount] = useState(0);
    
  return (
    <>
      <div className="grid">
        <button onClick={() => setCount((count) => count + 1)}>
          Increment Count
        </button>
        <button onClick={() => setCount((count) => count - 1)}>
          Decrement Count
        </button>
      </div>
      <div className="grid">
        <h3>Count is: {count}</h3>
      </div>
    </>
  );
}

export default Counter;