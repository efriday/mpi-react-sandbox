import { Pet, PetCard } from './ExerciseFive';
import useSWR from 'swr';
import Spinner from '../../shared/ui-components/Spinner';

export default function ExerciseFiveSolution() {
    return (
        <article id="exercise-five-solution">
            <PetSummary />
            <PetContainerBetter />
        </article>
    )
}

// Wrapper function to let us use the SWR library.
const fetcher = (...args: [input: RequestInfo | URL, init?: RequestInit | undefined]) => fetch(...args).then(res => res.json())

/**
 * `usePets` will act like a custom hook. It manages fetching the data and interpreting the response into something the UI cares about.
 * This lets our UI components all be developed against a standard set of states, and not have to worry about the data fetching.
 * SWR will automatically manage caching and re-fetching data for us. But we can override where necessary.
 */
function usePets() {
    const { data, error } = useSWR('https://pets-v2.dev-apis.com/pets', fetcher);

    return {
        pets: data?.pets,
        isLoading: !error && !data,
        isError: error
    }
}

function PetContainerBetter() {
    const { pets, isLoading, isError } = usePets();

    if (isError) return <ErrorComponent />;
    if (isLoading) return (<Spinner/>);

    return (
    <section id="better-pet-list">
        <p>We found {pets.length} pets available!</p>
        <ul>
            { pets?.map((pet: Pet) => <PetCard key={pet?.id} {...pet} />) }
        </ul>
    </section>)
}

/**
 * This is added as a demo of our ability to re-use the information and dervied state from the `usePets` hook.
 * It's not really necessary for this exercise, but it's a good example of how we can use the same data in multiple places and keep it synced...
 * *without* multiple API calls or a complex reducer.
 */
function PetSummary() {
    const { pets, isLoading, isError } = usePets();

    if (isLoading) return <Spinner />
    // If we return 'null' or 'undefined' inside a component, then it simply doesn't render or create anything on the dom. We'll ignore the summary for now.
    if (isError) return null;

    // I unpack the getBestCity function here to get the name and count. It has to happen *after* the isLoading and isError checks so it doesn't try to unpack invalid data.
    const [bestCityName, bestCityCount] = getBestCity(pets);

    return (
        <details>
            <summary role="button" className="secondary">Click to see the pet summary</summary>
            <ul>
                <li>Count: { pets.length }</li>
                <li>Dogs: { pets.filter((pet: Pet) => (pet.animal === 'dog'))?.length }</li>
                <li>Best City: { bestCityName } with { bestCityCount } pets available!</li>
            </ul>
        </details>
    );
}

/**
 * Ignore below. these are demo helper functions.
 */

const getBestCity = function(pets: Pet[]): [string, number] {
    const cityCounts = pets.reduce(getCityCount, {});
    return getHighestCount(Object.entries(cityCounts));
}

function getHighestCount(countables: [string, number][]) {
    return countables.reduce((prev, curr) => {
        if (prev[1] > curr[1]) return prev;
        return curr;
    });
}

function getCityCount(prev: { [key: string]: number }, curr: Pet) {
    const location = `${curr.city}, ${curr.state}`;
    if (prev[location]) {
        return {...prev, [location]: prev[location] + 1}
    }
    return {...prev, [location]: 1 };
}

function ErrorComponent() {
    return <div className="warning">Failed to load.</div>
}
