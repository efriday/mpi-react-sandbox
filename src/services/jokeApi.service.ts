export default class JokeAPI {
    public static async fetchJokes({count = 1, allowTwoPartJokes = false}): Promise<MultiOrSingleJokeResponse<GeneralJokeAPIResponse>> {
        const requestUrl = `https://v2.jokeapi.dev/joke/Any?safe-mode${allowTwoPartJokes ? '' : '&type=single'}${count > 1 ? `&amount=${count}` : ''}`;

        return fetch(requestUrl)
            .then((res) => res.json());
    }
}

/**
 * Ignore this stuff.
 */
export type MultiOrSingleJokeResponse<Type> = Type extends { amount: number, jokes: Joke[] } ? MultiJokeAPIResponse : IndividualJokeAPIResponse;

export type GeneralJokeAPIResponse = IndividualJokeAPIResponse | MultiJokeAPIResponse;

type BaseAPIResponse = {
    error: boolean;
}

export type MultiJokeAPIResponse = BaseAPIResponse & {
    amount: number;
    jokes:  Joke[];
}

export type IndividualJokeAPIResponse = BaseAPIResponse & Joke;

export type Joke = OnePartJoke | TwoPartJoke;

export interface TwoPartJoke extends BaseJoke {
    type: 'twopart';
    setup: string;
    delivery: string;
}

export interface OnePartJoke extends BaseJoke {
    type: 'single';
    joke: string;
}


interface BaseJoke {
    category:  string;
    type:      'single' | 'twopart';
    flags:     Flags;
    id:        number;
    safe:      boolean;
    lang:      string;
}

// These should always be false, but technically could be either.
export interface Flags {
    nsfw:      boolean;
    religious: boolean;
    political: boolean;
    racist:    boolean;
    sexist:    boolean;
    explicit:  boolean;
}
