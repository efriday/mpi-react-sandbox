import React, { useState } from "react";
import { Cat, catList } from "../../data/pets";
import { randomKey } from "../../shared/helpers/functions";

export default function ExerciseFour() {
    return (
        <article>
            <header>
                <hgroup>
                    <h3>Exercise Four - Forms & More Complex State </h3>
                    <p>Allow users to add a pet to the list of cats!</p>
                </hgroup>
            </header>
            {/* 
            NOTE: Your content should show in the element below named CatContainer. 
            The mock data is being provided in that component.
            */}
            <CatContainer />
        </article>
    );
}

/**
 *
 * Cat Object Shape - { name: string, animal: string, age: number, favoriteToy: string }
 */

function CatContainer() {
    const [cats, setCats] = useState(catList);

    return (
        <>
            <AddPetForm onAddPet={(pet: Cat) => setCats([...cats, pet])} />
            <section className="grid">
                {cats.map((cat: Cat) => (
                    <CatCard key={randomKey()} {...cat} />
                ))}
            </section>
        </>
    );
}

function CatCard({ name, animal, age, favoriteToy }: Cat) {
    return (
        <article>
            <img src="https://placekitten.com/200/200" alt="A Cat" />
            <h5>{name} - {animal}</h5>
            <p>
                Age: {age} years
                <br />
                Favorite Toy: {favoriteToy}
            </p>
        </article>
    );
}

function AddPetForm({ onAddPet }: { onAddPet: (pet: Cat) => void }) {
    const [petName, setPetName] = useState("");
    const [petAge, setPetAge] = useState(0);
    const [petFavoriteToy, setPetFavoriteToy] = useState("");

    /**
     * Functions to manage interacting with form elements
     */
    const handleChange = function ChangeHandler(
        setterMethod: (event: any) => void
    ) {
        return (event: any) => setterMethod(event.target.value);
    };

    const handleReset = function ResetHandler() {
        setPetName("");
        setPetAge(0);
        setPetFavoriteToy("");
    };
    const handleSubmit = function SubmitHandler(event: any) {
        // construct the output object.
        const newPet = {
            name: petName,
            animal: "cat" as const,
            age: petAge,
            favoriteToy: petFavoriteToy,
        };
        // console.log(`Creating a new pet: ${newPet}`);
        onAddPet(newPet);
        handleReset();
        event.preventDefault();
    };

    return (
        <section id="add-pet">
            <form onSubmit={handleSubmit} className="pet-form">
                <h4>Share your pet with the world!</h4>
                <label htmlFor="petName">Pet Name</label>
                <input
                    type="text"
                    name="petName"
                    placeholder="Enter A Name"
                    onChange={handleChange(setPetName)}
                    required
                />
                <label htmlFor="petAge">Age</label>
                <input
                    type="number"
                    name="petAge"
                    onChange={handleChange(setPetAge)}
                    required
                />
                <label htmlFor="petFavoriteToy">Favorite Toy</label>
                <input
                    type="text"
                    name="petFavoriteToy"
                    onChange={handleChange(setPetFavoriteToy)}
                    required
                />
                <div className="grid">
                    <input type="reset" value="Reset" onClick={handleReset} />
                    <input type="submit" value="Add Pet"></input>
                </div>
            </form>
        </section>
    );
}
