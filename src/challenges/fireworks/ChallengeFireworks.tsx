import { useState } from "react"
import Fireworks from "./Fireworks";

export default function ChallengeFireworks() {
    const [areFireworksEnabled, setAreFireworksEnabled] = useState(false);


    return (
        <>
          <button
            onClick={() => setAreFireworksEnabled(!areFireworksEnabled)}
            style={{ position: 'absolute', zIndex: 1 }}
          >
            {areFireworksEnabled ? 'Enabled' : 'Disabled'}
          </button>
          {areFireworksEnabled && (
            <Fireworks
              options={{ opacity: 0.5 }}
              style={{
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                position: 'fixed',
                background: '#000'
              }}
            />
          )}
        </>
      )
    }
