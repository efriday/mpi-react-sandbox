import { lazy, Suspense } from "react";
import { Link, RouteObject, useRoutes } from "react-router-dom";
import Header from "./shared/ui-components/Header";
import "./App.css";
import Spinner from "./shared/ui-components/Spinner";
import ChallengeFireworks from "./challenges/fireworks/ChallengeFireworks";

const Challenges = lazy(() => import("./challenges/Challenges"));
const ChallengePatient = lazy(() => import("./challenges/patient/ChallengePatient"));
const ExerciseOne = lazy(() => import("./exercises/1/ExerciseOne"));
const ExerciseTwo = lazy(() => import("./exercises/2/ExerciseTwo"));
const ExerciseThree = lazy(() => import("./exercises/3/ExerciseThree"));
const ExerciseFour = lazy(() => import("./exercises/4/ExerciseFour"));
const ExerciseFive = lazy(() => import("./exercises/5/ExerciseFive"));
const Exercises = lazy(() => import("./exercises/Exercises"));
const ModernJS = lazy(() => import("./exercises/modern-js/ModernJS"));
const MJSOverview = lazy(() => import("./exercises/modern-js/overview/MJSOverview"));
const ScopeAndClosures = lazy(() => import("./exercises/modern-js/scope-and-closures/ScopeAndClosures"));
const VanillaJS = lazy(() => import("./exercises/modern-js/vanilla-js/VanillaJS"));

function App() {
    let routedElement = useRoutes(ROUTES);

    return (
        <>
            <Header />
            <main className="container">{routedElement}</main>
        </>
    );
}

export default App;

function WithSpinnerFallback({ children }: { children: JSX.Element }) {
    return <Suspense fallback={<Spinner />}>{children}</Suspense>;
}

const ROUTES: RouteObject[] = [
    {
        path: "/",
        children: [
            // { index: true, element: <Home /> },
            {
                path: "/exercises",
                element: <Suspense fallback={<Spinner />}><Exercises /></Suspense>,
                children: [
                    // { index: true, element: <CoursesIndex /> },
                    { path: "/exercises/1", element: <Suspense fallback={<Spinner />}><ExerciseOne /></Suspense> },
                    { path: "/exercises/2", element: <Suspense fallback={<Spinner />}><ExerciseTwo /></Suspense> },
                    { path: "/exercises/3", element: <Suspense fallback={<Spinner />}><ExerciseThree /></Suspense> },
                    { path: "/exercises/4", element: <Suspense fallback={<Spinner />}><ExerciseFour /></Suspense> },
                    { path: "/exercises/5", element: <Suspense fallback={<Spinner />}><ExerciseFive /></Suspense> },
                ],
            },
            {
                path: "/challenges",
                element: <Challenges />,
                children: [
                    { path: "/challenges/1", element: <Suspense fallback={<Spinner />}><ChallengePatient /></Suspense> },
                    { path: "/challenges/6", element: <Suspense fallback={<Spinner />}><ChallengeFireworks /></Suspense> },
                ],
            },
            {
                path: "/modern-js",
                element: <Suspense fallback={<Spinner />}><ModernJS /></Suspense>,
                children: [
                    { index: true, element: <Suspense fallback={<Spinner />}><MJSOverview /></Suspense> },
                    { path: "/modern-js/vanilla", element: <Suspense fallback={<Spinner />}><VanillaJS /></Suspense> },
                    {
                        path: "/modern-js/scope-and-closures",
                        element: <Suspense fallback={<Spinner />}><ScopeAndClosures /></Suspense>,
                    },
                ],
            },
            { path: "*", element: <NoMatch /> },
        ],
    },
];

export function NoMatch() {
    return (
        <article>
            <h2>Page Not Found</h2>
            <p>
                <Link to="/">Go to the home page</Link>
            </p>
        </article>
    );
}
